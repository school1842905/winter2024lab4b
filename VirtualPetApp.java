import java.util.Scanner;
public class VirtualPetApp{

	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		/*Whale num1 = new Whale("Blue Whale", 130000.00, false);
		Whale num2 = new Whale("Orca", 3000.00, true);
		Whale num3 = new Whale("Sperm Whale", 41000.00, true);
		
		System.out.println(num1.whaleFacts() + num1.weightConverter());
		System.out.println(num2.whaleFacts() + num1.weightConverter());
		System.out.println(num3.whaleFacts() + num1.weightConverter());*/
		
		Whale[] marineMammals = new Whale[1];
		
			for(int i = 0; i < marineMammals.length; i++){
				System.out.println("What specie?");
				String species = reader.nextLine();
				System.out.println("How heavy is it in kg?");
				double weight = reader.nextDouble();
				System.out.println("Is it teethed: answer by true or false");
				boolean teeth = reader.nextBoolean();
				reader.nextLine();
				marineMammals[i] = new Whale(species, weight, teeth);
				System.out.println("Wow! What amazing stats for a " + marineMammals[i].getSpecies() + "!" );
			}
			
			boolean teethPresent = reader.nextBoolean();
			
			final int last = marineMammals.length - 1;
			System.out.println(marineMammals[last].getTeeth());
			marineMammals[last].setTeeth(teethPresent);
			System.out.println(marineMammals[last].getTeeth());
			
			//for (int i = 0; i < marineMammals.length; i++){
			//	System.out.println(marineMammals[i].whaleFacts() + marineMammals[i].weightConverter());		
			//}
		
	}
}