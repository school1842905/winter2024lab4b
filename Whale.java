public class Whale{

	private String species;
	private double weight; 
	private boolean teeth;
	
	public Whale(String species, double weight, boolean teeth){
		this.species = species;
		this.weight = weight;
		this.teeth = teeth;
	}
	
	// instance methods
	
	public String whaleFacts(){
		if(teeth){
			return "The " + species + " is a toothed whale which means it has teeth instead of sieves";
		}
		else{
			return "The " + species + "is a baleen whale which means it has sieves instead of teeth";
		}
	}

	public String weightConverter(){
		double inPounds = weight * 2.2;
			return " The " + species + " weights " + weight + " in kg and " + inPounds + " in pounds";	
	}
	
	// get methods
	
	public String getSpecies(){
		return this.species;
	}
	
	public double getWeight(){
		return this.weight;	
	}
	
	public boolean getTeeth(){
		return this.teeth;
	}
	
	// set methods
	
	public void setSpecies(String newSpecies){
		this.species = newSpecies;
	}
	
	public void setWeight(double newWeight){
		this.weight = newWeight;
	}
	
	public void setTeeth(boolean newTeeth){
		this.teeth = newTeeth;
	}
	
	
}